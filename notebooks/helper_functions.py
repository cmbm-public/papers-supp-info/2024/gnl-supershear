import sys
sys.path.append('../external/database-manager/')
import dbmanager as dm

import matplotlib.pyplot as plt
from IPython.display import clear_output
    
sys.path.append('../external/rupturehunter/')
sys.path.append('../external/')
import rupturehunter as rh
import solidmechanics as sm
import solidmechanics.lefm as lefm

import fenics as fe

def get_griffiths_length(job_id):
    matprop = sm.LinearElasticMaterial({sm.smd.E:  schema.job.digParameterValue('E', job_id), 
                                    sm.smd.nu: schema.job.digParameterValue('poisson', job_id), 
                                    sm.smd.rho: schema.job.digParameterValue('rho', job_id), 
                                    sm.smd.pstress:False})
    
    # plane stress vs. plane strain
    E_eff = matprop[sm.smd.E]
    if matprop.is_plane_strain():
        E_eff /= (1-matprop[sm.smd.nu]**2)

    sigma_inf = schema.job.digParameterValue('prestrain', job_id)*schema.job.digParameterValue('E', job_id)
    
    # Griffith's length
    griffiths_length = lefm.compute_critHalfLength(schema.job.digParameterValue('Gamma', job_id),
                                     sigma_inf, 
                                     matprop=matprop)

    return griffiths_length

def get_equation_of_motion(results, job_id):
    matprop = sm.LinearElasticMaterial({sm.smd.E:  schema.job.digParameterValue('E', job_id), 
                                    sm.smd.nu: schema.job.digParameterValue('poisson', job_id), 
                                    sm.smd.rho: schema.job.digParameterValue('rho', job_id), 
                                    sm.smd.pstress:False})
    # plane stress vs. plane strain
    E_eff = matprop[sm.smd.E]
    if matprop.is_plane_strain():
        E_eff /= (1-matprop[sm.smd.nu]**2)

    sigma_inf = schema.job.digParameterValue('prestrain', job_id)*schema.job.digParameterValue('E', job_id)
 
        
    # solution is given per speed (instead of space): define speed range of interest
    cfs = np.linspace(0,matprop[sm.smd.cR],1001)[:-1]
    gIs = lefm.compute_g(cfs,matprop,1) # mode I

    # find associated crack half-length
    a = schema.job.digParameterValue('Gamma', job_id) * E_eff / np.pi / gIs / (sigma_inf)**2
    nans = np.isnan(a)
    a = a[~nans]
    cfs = cfs[~nans]
    
    indexes = np.argsort(results['coords.x'][0])
    coordinates_along_interface = results['coords.x'][0, indexes].reshape(-1)  

    # reduce to area of interest
    fltr_max = np.where(a < max(coordinates_along_interface))[0]
    fltr_min = np.where(a > min(coordinates_along_interface))[0]
    fltr = np.intersect1d(fltr_min, fltr_max)

    # return to space of interest
    eom = np.zeros((len(fltr),2))
    eom[:,0] = a[fltr]
    eom[:,1] = cfs[fltr]
    
    return eom

def get_static_cohesive_zone_size(results, job_id):

    matprop = sm.LinearElasticMaterial({sm.smd.E:  schema.job.digParameterValue('E', job_id), 
                                    sm.smd.nu: schema.job.digParameterValue('poisson', job_id), 
                                    sm.smd.rho: schema.job.digParameterValue('rho', job_id), 
                                    sm.smd.pstress:False})
    # plane stress vs. plane strain
    E_eff = matprop[sm.smd.E]
    if matprop.is_plane_strain():
        E_eff /= (1-matprop[sm.smd.nu]**2)

    iface = {sm.smd.Gamma : schema.job.digParameterValue('Gamma', job_id), sm.smd.tauc : schema.job.digParameterValue('sigma_c', job_id), sm.smd.taur : 0 }
    
    return lefm.cohesive_zone.compute_Xc0(matprop, iface)


def plot_space_time_cohesive(ax, results, job_id):
    indexes = np.argsort(results['coords.x'][0])
    coordinates_along_interface = results['coords.x'][0, indexes].reshape(-1)  #np.linspace(0,0.01,200)

    griffiths_length = get_griffiths_length(job_id)
    cohesive_zone_size = get_static_cohesive_zone_size(results, job_id)
    padded = int(griffiths_length/( cohesive_zone_size/schema.job.digParameterValue('nb_elem_coh', job_id)))

    normalized_coordinates_along_interface = coordinates_along_interface/griffiths_length
    
    min_length = np.minimum(results['tip.crack'][:, indexes].shape[0], results['tip.cohesive'][:, indexes].shape[0])
    
    temp = 1-results['tip.crack'][:min_length, indexes] + results['tip.cohesive'][:min_length, indexes]
    padded_temp = np.pad(temp, ((0, 0), (padded, 0)), constant_values=(2))
    
    ax.imshow(padded_temp, origin='lower', 
             extent=[0, np.max( normalized_coordinates_along_interface), np.min(results['timings'])*1e3, np.max(results['timings'])*1e3], aspect='auto')

def get_tip_speed(results, job_id, tip='cohesive'):
    # Griffith's length
    griffiths_length = get_griffiths_length( job_id)
    
    matprop = sm.LinearElasticMaterial({sm.smd.E:  schema.job.digParameterValue('E', job_id), 
                                    sm.smd.nu: schema.job.digParameterValue('poisson', job_id), 
                                    sm.smd.rho: schema.job.digParameterValue('rho', job_id), 
                                    sm.smd.pstress:False})
      
    indexes = np.argsort(results['coords.x'][0])
    if tip == 'cohesive':
        openings_bool = results['tip.cohesive'][:, indexes] == 0
    elif tip == 'crack':
        openings_bool = results['tip.crack'][:, indexes] == 1

    nb_steps = openings_bool.shape[0]

  
    hunter = rh.RuptureHunter()
    hunter.load(results['coords.x'][0, indexes].reshape(1, len(results['coords.x'][0, :])), results['timings'][:nb_steps], 
                openings_bool[:nb_steps, :])
    hunter.hunt()
    rupture = hunter.get_rupture(hunter.get_rupture_indexes()[0])
    front = rupture.get_sorted_front()
    
    speed = rupture.get_propagation_speed(avg_dist=2.5*griffiths_length)
    
    return speed

def plot_tip_speed(ax, results, job_id, color, strain, deform, tip='cohesive', marker='o', markerfacecolor='none', markeredgecolor='k', markeredgewidth=0.2, ms=5, markevery=5):
    stretch = 1 + schema.job.digParameterValue('prestrain', job_id)

    # Griffith's length
    griffiths_length = get_griffiths_length( job_id)
    
    matprop = sm.LinearElasticMaterial({sm.smd.E:  schema.job.digParameterValue('E', job_id), 
                                    sm.smd.nu: schema.job.digParameterValue('poisson', job_id), 
                                    sm.smd.rho: schema.job.digParameterValue('rho', job_id), 
                                    sm.smd.pstrain:True})
  
    
   
    indexes = np.argsort(results['coords.x'][0])
    if tip == 'cohesive':
        openings_bool = results['tip.cohesive'][:, indexes] == 0
    elif tip == 'crack':
        openings_bool = results['tip.crack'][:, indexes] == 1

    nb_steps = openings_bool.shape[0]

  
    hunter = rh.RuptureHunter()
    hunter.load(results['coords.x'][0, indexes].reshape(1, len(results['coords.x'][0, :])), results['timings'][:nb_steps], 
                openings_bool[:nb_steps, :])
    hunter.hunt()
    rupture = hunter.get_rupture(hunter.get_rupture_indexes()[0])
    front = rupture.get_sorted_front()
    
    speed = rupture.get_propagation_speed(avg_dist=2.5*griffiths_length)
    
    cut_off_index = 1
    if strain == 0.125 and deform == 'large':
        cut_off_index = np.where(speed[:, 0]/griffiths_length > 4)[0][0]

    ax.plot((speed[cut_off_index:, 0])/griffiths_length, speed[cut_off_index:, 2]/matprop[sm.smd.cR], marker, ls='solid', color=color,
            ms=ms, markeredgecolor=markeredgecolor, markeredgewidth=markeredgewidth, markerfacecolor=markerfacecolor, markevery=markevery,
            label=r'$\mathrm{Simulation}, \lambda=$' + str(np.round(stretch, 2)), zorder=2)
    ax.set_xlim(left=0)
    
    # equation of motion
    eom = get_equation_of_motion(results, job_id)
    ax.plot(eom[:,0]/griffiths_length, eom[:,1]/matprop[sm.smd.cR], label=r'$\mathrm{Exact~solution}$', color='xkcd:salmon', zorder=3, lw=1.2)