# Supplementary material
-------------------------

## Title 
Transonic and Supershear Crack Propagation Driven by Geometric Nonlinearities 
[paper](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.132.226102)

## Authors
Mohit Pundir, Mokhtar Adda-Bedia, and David S. Kammer

## Date
31-05-2024

## Description

The repository contains the script and notebooks to reproduce the results and the figures used in the paper. 

## Scripts

- File **simulations/run-cohesive.py** is the executable  used to setup the input files for the respective simuilations.
- File **simulations/cohesive_zone.py** is the executable which produces the respective results.
- File **notebooks/analysis.ipynb** is the notebook that can be used to read the generated data and reroduce the figures. 

One can run the scripts in the simulation directory to generate the data and then use the notebook provided to analyse the generated data.

## Data availabilty
One can also use the data used in the paper and run the notebook on this data.  The data used in the paper (and in the notebooks here) can be downloaded from the ETH Research collection (.......).


## Dependencies

Here is the list of dependencies to run the simulations:

- FEniCS 2019 (For Finite Element framework)
- GMSH (v4.4.1) For creating meshes

The following dependencies are added as submodules to the exisitng repo.

- cohesive-element-formulations (https://gitlab.ethz.ch/compmechmat/research/fracture/cohesive-element-formulations)

    A small library build upon FEniCS for phasefield formulation of static and dynamic cracks

- RuptureHunter (https://gitlab.ethz.ch/compmechmat/research/libs/rupturehunter)

    For calculating the crack speed.


- SolidMechanics (https://gitlab.ethz.ch/compmechmat/research/libs/solidmechanics)

    For various solid mechanics and lefm functions.

- Database-Manager (https://gitlab.com/mohitpundir/database-manager.git)

    For managing simulation data

## Usage/Installation

Clone the repository using git clone
To download all the dependencies (except FEniCS, GMSH) as submodules, run the following command

```
git submodule update --init --recursive
```

For the other dependencies (FEniCS, GMSH), please follow the instructions as given in their respective documentation.