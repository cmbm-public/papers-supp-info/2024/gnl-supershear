import argparse
import os
import subprocess
from dolfin_utils.meshconvert import meshconvert

class LoadFromFile (argparse.Action):
    def __call__ (self, parser, namespace, values, option_string = None):
        with values as f:
            # parse arguments in the file and store them in the target namespace
            parser.parse_args(f.read().split(), namespace)


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')



def create_mesh(length, height, lc, min_elem,
                input_file, output_file, mesh_filename):
    open(input_file, 'w').write(
    f"""
h = 1;

x = {length}; // meters
y = {height}; // meters

a = {lc};  //meters

epsilon = 1e-5;

element_size = {min_elem}; //meters

nb_points_on_strong = (x-a)/element_size + 1;
nb_points_on_crack = a/element_size + 1;
nb_points_on_top = nb_points_on_strong + nb_points_on_crack - 1;
nb_points_on_side = (y)/element_size/2 + 1;


Point(5) = {{0,  epsilon, 0, h}};
Point(8) = {{0,  -epsilon, 0, h}};

Point(9) = {{a, 0, 0, h}};
Point(10) = {{0, y/2, 0, h}};
Point(11) = {{0, -y/2, 0, h}};
Point(12) = {{x, y/2, 0, h}};
Point(13) = {{x, -y/2, 0, h}};
Point(14) = {{x, 0, 0, h}};

Line(3) = {{10, 12}};

Line(5) = {{10, 5}};
Line(6) = {{5, 9}};
Line(7) = {{9, 14}};
Line(8) = {{14, 12}};


Line(11) = {{8, 11}};
Line(12) = {{11, 13}};
Line(13) = {{13, 14}};
Line(14) = {{9, 8}};


Line Loop(2) = {{5, 6, 7, 8, -3}};
Line Loop(4) = {{-7, 14, 11, 12, 13}};

Plane Surface(2) = {{2}};
Plane Surface(4) = {{4}};

Physical Line(2) = {{7}}; 
Physical Surface(1) = {{2, 4}};

Transfinite Line {{7}} = nb_points_on_strong Using Progression 1;
Transfinite Line {{6, 14}} = nb_points_on_crack Using Progression 1;
Transfinite Line {{3, 12}} = nb_points_on_top Using Progression 1;
Transfinite Line {{5, 8, 11, 13}} = nb_points_on_side Using Progression 1;


Transfinite Surface "*";
Transfinite Surface {{2}} = {{12, 10, 5, 14}};
Transfinite Surface {{4}} = {{14, 8, 11, 13}};

"""
)

    ret = subprocess.run(f"""gmsh -2 -format msh2 -o {output_file} {input_file}""", shell=True)
    if ret.returncode:
        print("Beware, gmsh could not run: mesh is not regenerated")
    else:
        print("Mesh generated")

    
    meshconvert.convert2xml(output_file, 
                            mesh_filename, 
                            iformat='gmsh')
