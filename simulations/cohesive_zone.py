import os
import sys

current_dir = os.path.dirname(__file__)
base_dir = os.path.join(os.path.dirname(__file__), "..")

sys.path.append(os.path.join(base_dir, "external/database-manager/"))
from dbmanager import Manager

sys.path.append(os.path.join(base_dir, "external/cohesive-element-formulations/"))
import cohesive_model as cm

sys.path.append(os.path.join(base_dir, "external/"))
import solidmechanics as sm
import solidmechanics.lefm as lefm


import numpy as np
import fenics as fe
from utils import *
from mpi4py import MPI

import argparse

solver_options = {
     'linear' : { 'type' : 'linear', 
                 'parameters' : {
                            'linear_solver' : 'cg',
                            'relative_tolerance' : 1e-6,
                            'absolute_tolerance' : 1e-6,
                            'maximum_iterations' : 50}},
    'nonlinear' : { 'type' : 'nonlinear',
                   'parameters' :   {
                       "nonlinear_solver": "snes",
                       "snes_solver": {
                           "linear_solver": "gmres",
                           "preconditioner": "amg",
                           "maximum_iterations": 50,
                           "relative_tolerance": 1e-6,
                           "absolute_tolerance": 1e-6,
                           "krylov_solver" : {
                               'absolute_tolerance' : 1e-5,
                               "monitor_convergence" : True,
                           },
                           "report": False,
                           "error_on_nonconvergence": True}}}}


# Form compiler options
fe.set_log_level(40)    # remove unnecessary console outputs

fe.parameters["form_compiler"]["cpp_optimize"] = True
fe.parameters["form_compiler"]["optimize"] = True
fe.parameters["ghost_mode"] = "shared_facet"
fe.parameters['allow_extrapolation'] = True
fe.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

# Use Chebyshev smoothing for multigrid
fe.PETScOptions.set("mg_levels_ksp_type", "chebyshev")
fe.PETScOptions.set("mg_levels_pc_type", "jacobi")
            
# Improve estimate of eigenvalues for Chebyshev smoothing
fe.PETScOptions.set("mg_levels_esteig_ksp_type", "cg")
fe.PETScOptions.set("mg_levels_ksp_chebyshev_esteig_steps", 50)


# ------------------------------------------- ##
# Input arguments
# ------------------------------------------- ##
parser = argparse.ArgumentParser()
parser.add_argument('--prefactor',         type=float, default=1.1)
parser.add_argument('--nb_elem_coh',       type=float, default=10)
parser.add_argument('--time',              type=float, default=1e-2)
parser.add_argument('--E',                 type=float, default=106e3)
parser.add_argument('--poisson',           type=float, default=0.35)
parser.add_argument('--Gamma',             type=float, default=5)
parser.add_argument('--rho',               type=float, default=1025)
parser.add_argument('--prestrain',         type=float, default=6e3)
parser.add_argument('--sigma_c',           type=float, default=1e4)
parser.add_argument('--time_factor',       type=float, default=0.1)
parser.add_argument('--alpha',             type=float, default=100)
parser.add_argument('--length_factor',     type=float, default=10)
parser.add_argument('--height_factor',     type=float, default=20)
parser.add_argument('--mesh'        ,      type=str,   default='unstructured')  
parser.add_argument('--deformations',      type=str,   default='small')
parser.add_argument('--material_law',      type=str,   default='linear')
parser.add_argument('--idx',               type=str,   default='small-deformations')
parser.add_argument('--path',              type=str,   default='./')
parser.add_argument('--schema',            type=str,   default='trial') 
parser.add_argument('--file',              type=open,  action=LoadFromFile)
args = parser.parse_args()


comm = MPI.COMM_WORLD
rank = comm.Get_rank()


manager = Manager(args.path, args.schema,
                  uid=args.idx, comm=comm)
manager.initialize(parameters=vars(args))
                     
database_location = manager.getDatabaseLocation()

# material and fracture properties
if args.material_law == 'linear':
    material = cm.LinearElasticMaterial(dim=2,
                                        deformations=args.deformations)
    static_material = cm.LinearElasticMaterial(dim=2,
                                               deformations=args.deformations)
elif args.material_law == 'st-venant':
    material = cm.StVenantKirchoffMaterial(dim=2,
                                        deformations='small')
    static_material = cm.StVenantKirchoffMaterial(dim=2,
                                                  deformations='small')
else:
    raise RuntimeError('Material defined is wrong', args.material_law)


material.E = args.E
material.rho = args.rho
material.poisson = args.poisson
material.model_type = 'plane_strain'
material.setInternals()

static_material.E = args.E
static_material.rho = args.rho
static_material.poisson = args.poisson
static_material.model_type = 'plane_strain'
static_material.setInternals()


# Griffith's length
matprop = sm.LinearElasticMaterial({sm.smd.E:  args.E, 
                                    sm.smd.nu: args.poisson, 
                                    sm.smd.rho: args.rho, 
                                    sm.smd.pstress:False})
sigma_inf = args.prestrain*args.E
lc = args.prefactor*lefm.compute_critHalfLength(args.Gamma,sigma_inf,matprop=matprop)
print('Griffiths length | {}'.format(lc))


mesh_filename = database_location + '/mesh.xml'
mesh = fe.Mesh(mesh_filename)


## boundaries
y_max = np.max(mesh.coordinates()[:, 1])
y_min = np.min(mesh.coordinates()[:, 1])
x_min = np.min(mesh.coordinates()[:, 0])
x_max = np.min(mesh.coordinates()[:, 0])

comm.barrier()

y_max = mesh.mpi_comm().allreduce(y_max, op=MPI.MAX)
y_min = mesh.mpi_comm().allreduce(y_min, op=MPI.MIN)
x_min = mesh.mpi_comm().allreduce(x_min, op=MPI.MIN)
x_max = mesh.mpi_comm().allreduce(x_max, op=MPI.MAX)

height = y_max - y_min
length = x_max - x_min


top = fe.CompiledSubDomain("near(x[1], h) && on_boundary",  h=height/2)
bot = fe.CompiledSubDomain("near(x[1], -h) && on_boundary", h=height/2)
left = fe.CompiledSubDomain("near(x[0], h) && on_boundary", h=0)
right = fe.CompiledSubDomain("near(x[0], h) && on_boundary", h=length)


static_model = cm.QuasiStaticFormulation(mesh,
                                         material=static_material,
                                         penalty=args.alpha)
static_model.initialize()

prestrain_top_expression =   fe.Expression(( "u*t") , u=args.prestrain*height/2,
                                           t=0,
                                           degree=0)

prestrain_bot_expression =   fe.Expression(( "-u*t") , u=args.prestrain*height/2,
                                           t=0,
                                           degree=0)

prestrain_bot = fe.DirichletBC(static_model.W.sub(1),
                               prestrain_bot_expression,
                               bot, 
                               method='geometric')
                             
prestrain_top = fe.DirichletBC(static_model.W.sub(1),
                               prestrain_top_expression,
                               top, 
                               method='geometric')

    
#prestrain_bot = fe.DirichletBC(static_model.W.sub(1),
#                               fe.Expression(("-u"), u=args.prestrain*height/2,
#                                             degree=0), bot, 
#                               method='geometric')
#prestrain_top = fe.DirichletBC(static_model.W.sub(1),
#                               fe.Expression(( "u") , u=args.prestrain*height/2,
#                                             degree=0), top, 
#                               method='geometric')
symm_left = fe.DirichletBC(static_model.W.sub(0),
                           fe.Constant((0.0)) , left,    method='geometric')


static_model.bc_u = [prestrain_bot, prestrain_top, symm_left]
static_model.setProblemAndSolver(solver_options=solver_options.get('nonlinear'))

#static_model.solve()
loadings = np.linspace(0, 1, 11)

for i in loadings[1:]:
    if rank == 0:
        print('static loading ', i)

    prestrain_top_expression.t = i
    prestrain_bot_expression.t = i
    static_model.solve()


class CohesiveInterface(fe.SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[1]) < fe.DOLFIN_EPS

linear_cohesive_law = cm.LinearCohesiveLaw(Gc=args.Gamma, sig=args.sigma_c)
    
explicit_model = cm.ExplicitFormulation(mesh,
                                        material=material, 
                                        penalty=args.alpha)

time_step = explicit_model.getCriticalTimeStep()
comm.barrier()
time_step = comm.allreduce(time_step, op=MPI.MIN)
explicit_model.dt = time_step*args.time_factor

explicit_model.markCohesiveInterface({2 : CohesiveInterface()})

extrinsic_cohesive_model = cm.ExtrinsicCohesiveFormulation(explicit_model,
                                                           linear_cohesive_law,
                                                           interfaces=[2])
extrinsic_cohesive_model.initialize()

explicit_model.bc_u = [fe.DirichletBC(explicit_model.W,
                                      static_model.u_new, bot,
                                      method="geometric"),
                       fe.DirichletBC(explicit_model.W,
                                      static_model.u_new, top,
                                      method="geometric"),
                       #fe.DirichletBC(explicit_model.W.sub(0),
                       #               static_model.u_new.sub(0), left,
                       #               method="geometric"), 
                       fe.DirichletBC(explicit_model.W,
                                      static_model.u_new, right,
                                      method="geometric")]

explicit_model.u_old.vector()[:] = static_model.u_new.vector().get_local()[:]
explicit_model.u_new.vector()[:] = static_model.u_new.vector().get_local()[:]


# ------------------------------------------- ##
# Simulations
# ------------------------------------------- ##


time_max = args.time
Nincr = int(time_max/float(explicit_model.dt))
loading = np.linspace(0, time_max, Nincr)

# ------------------------------------------- ##
# Dumper
# ------------------------------------------- ##

facet_coords = cm.computeFacetCoordinates(extrinsic_cohesive_model.V_f2,
                                          dim=2,
                                          dS=explicit_model.dS)
fc = facet_coords.vector().get_local().reshape(
    len(extrinsic_cohesive_model.g_max.vector()), 2)


interface_indexes= np.argwhere( (fc[:, 1] == 0) & (fc[:, 0] > lc) ).reshape(-1)
x_positions = fc[interface_indexes, 0].reshape(-1)
sorted_interface_indexes = [x for _,x in sorted(zip(x_positions, interface_indexes))]


outputs={'coords.x'         : np.zeros((1, len(x_positions))),
         'gap.maximum'      : np.zeros((1, len(x_positions))),
         'gap.effective'    : np.zeros((1, len(x_positions))),
         'gap.ctod'         : np.zeros((1, len(x_positions))),
         'tractions.applied': np.zeros((1, len(x_positions))),
         'tractions.actual' : np.zeros((1, len(x_positions))),
         'tip.crack'        : np.zeros((1, len(x_positions))),
         'tip.cohesive'     : np.zeros((1, len(x_positions))),
         'timings'          : float(0),
         'energy.elastic'   : float(0),
         'energy.kinetic'   : float(0)
         }

manager.registerQuantities(outputs)


x_sorted = np.sort(x_positions)

outputs['coords.x'] = x_sorted.reshape((1, len(x_sorted)))


outputs['coords.x'] = x_sorted.reshape((1, len(x_sorted)))



dumper = fe.XDMFFile(comm, database_location + "/output.xdmf")
dumper.parameters["flush_output"] = True
dumper.parameters["functions_share_mesh"] = True

Vsig = fe.TensorFunctionSpace(mesh, "DG", degree=0)
sig_cauchy = fe.Function(Vsig, name="Cauchy Stress")
sig_pk2 = fe.Function(Vsig, name='PK2 Stress')


Veng = fe.FunctionSpace(mesh, "DG", 1)
energy_density = fe.Function(Veng, name='Energy density')
kinetic_density = fe.Function(Veng, name='Kinetic density')


Vpon = fe.VectorFunctionSpace(mesh, "DG", degree=0)
poynting_cauchy = fe.Function(Vpon, name='Poynting cauchy')
poynting_pk2 = fe.Function(Vpon, name='Poynting pk2')


sig_cauchy.assign(fe.project(material.sigma_cauchy(explicit_model.u_old), Vsig))
sig_pk2.assign(fe.project(material.sigma_pk2(explicit_model.u_old), Vsig))

energy_density.assign(fe.project(0.5*fe.inner(material.sigma_cauchy(explicit_model.u_old),
                                    material.epsilon_small(explicit_model.u_old))))
kinetic_density.assign(fe.project(0.5*material.rho*fe.inner(explicit_model.v_new, explicit_model.v_new)))



poynting_cauchy.assign(fe.project(fe.dot(material.sigma_cauchy(explicit_model.u_old),
                                         explicit_model.v_new)))

poynting_pk2.assign(fe.project(fe.dot(material.sigma_pk2(explicit_model.u_old),
                                      explicit_model.v_new)))


dumper.write(sig_cauchy, 0)
dumper.write(sig_pk2, 0)
dumper.write(energy_density, 0)
dumper.write(kinetic_density, 0)
dumper.write(poynting_cauchy, 0)
dumper.write(poynting_pk2, 0)



for (i, t) in enumerate(loading[1:]):
    print("Load step | ", i+1, t)
   
    
    extrinsic_cohesive_model.solve()

    # saving outputs
    outputs['timings'] = t
    outputs['gap.ctod'] =  extrinsic_cohesive_model.ctod.vector()[sorted_interface_indexes].reshape((1, len(x_sorted)))
    outputs['gap.maximum'] =  extrinsic_cohesive_model.g_max.vector()[sorted_interface_indexes].reshape((1, len(x_sorted)))
    outputs['gap.effective']  =  extrinsic_cohesive_model.g_eff.vector()[sorted_interface_indexes].reshape((1, len(x_sorted)))
    outputs['tractions.actual'] = extrinsic_cohesive_model.traction.vector()[sorted_interface_indexes].reshape((1, len(x_sorted)))
    outputs['tip.crack']  = extrinsic_cohesive_model.cracked_zone.vector()[sorted_interface_indexes].reshape((1, len(x_sorted)))
    outputs['tip.cohesive'] = extrinsic_cohesive_model.cohesive_zone.vector()[sorted_interface_indexes].reshape((1, len(x_sorted))) 

    outputs['energy.elastic'] = explicit_model.getEnergy('elastic')
    outputs['energy.kinetic'] = explicit_model.getEnergy('kinetic')

    if i%100 == 0:
        sig_cauchy.assign(fe.project(material.sigma_cauchy(explicit_model.u_old), Vsig))
        sig_pk2.assign(fe.project(material.sigma_pk2(explicit_model.u_old), Vsig))
        
        energy_density.assign(fe.project(0.5*fe.inner(material.sigma_cauchy(explicit_model.u_old),
                                                      material.epsilon_small(explicit_model.u_old))))
        kinetic_density.assign(fe.project(0.5*material.rho*fe.inner(explicit_model.v_new, explicit_model.v_new)))


        
        poynting_cauchy.assign(fe.project(fe.dot(material.sigma_cauchy(explicit_model.u_old),
                                                 explicit_model.v_new)))

        poynting_pk2.assign(fe.project(fe.dot(material.sigma_pk2(explicit_model.u_old),
                                              explicit_model.v_new)))

        dumper.write(sig_cauchy, t)
        dumper.write(sig_pk2, t)
        dumper.write(energy_density, t)
        dumper.write(kinetic_density, t)
        dumper.write(poynting_cauchy, t)
        dumper.write(poynting_pk2, t)
        #dumper.write_checkpoint(explicit_model.u_new, 'Displacement', t, fe.XDMFFile.Encoding.HDF5, True)
        #dumper.write_checkpoint(poynting_cauchy, 'Poynting Cauchy', t, fe.XDMFFile.Encoding.HDF5, True)
        #dumper.write_checkpoint(poynting_pk2, 'Poynting PK2', t, fe.XDMFFile.Encoding.HDF5, True)
                
        time_table = fe.timings(fe.TimingClear.keep, [fe.TimingType.wall])
        with open(database_location + "/timings.log", "w") as out:
            out.write(time_table.str(True))



    manager.pushQuantity(outputs)


manager.finalize()
