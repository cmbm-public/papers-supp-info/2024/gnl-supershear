from utils import *
import sys

current_dir = os.path.dirname(__file__)
base_dir = os.path.join(os.path.dirname(__file__), "..")

sys.path.append(os.path.join(base_dir, "external/"))

import solidmechanics as sm
import solidmechanics.lefm as lefm
from dolfin_utils.meshconvert import meshconvert


def create_unstructured_mesh(length, height, lc, min_elem,
                             input_file, output_file, mesh_filename):
    open(input_file, 'w').write(
    f"""
SetFactory("OpenCASCADE");

Mesh.CharacteristicLengthFromPoints = 0;
Mesh.CharacteristicLengthFromCurvature = 0;
Mesh.CharacteristicLengthExtendFromBoundary = 0;

h2 = 1;
L = {length};
H = {height};
l = {lc};
epsilon = 1e-5;
depth = 0;


Point(1) = {{0, epsilon, -depth/2, h2}};
Point(2) = {{l, 0, -depth/2, h2}};

Point(6) = {{0, -epsilon, -depth/2, h2}};
Point(7) = {{0, -H/2, -depth/2, h2}};
Point(8) = {{L, -H/2, -depth/2, h2}};
Point(9) = {{L, H/2, -depth/2, h2}};
Point(10) = {{0, H/2, -depth/2, h2}};
Point(11) = {{L, 0, -depth/2, h2}};



Line(1) = {{1, 2}};
Line(2) = {{2, 11}};
Line(6) = {{11, 9}};
Line(7) = {{9, 10}};
Line(8) = {{10, 1}};

Line(9) = {{2, 6}};
Line(10) = {{6, 7}};
Line(11) = {{7, 8}};
Line(12) = {{8, 11}};


Line Loop(1) = {{1, 2, 6, 7, 8}};
Line Loop(2) = {{9, 10, 11 ,12, -2}};
Plane Surface(1) = {{1}};
Plane Surface(2) = {{2}};


Physical Line(2) = {{2}};
Physical Surface(1) = {{1, 2}};
Coherence;

// Definition of the refinement box
MeshSizeInsideBox={min_elem}; // Mesh Size inside the refinement box
MeshSizeOutsideBox={min_elem*10}; //  Mesh Size outside the refinement box 'domain'

PosBoxXMin = 0;
PosBoxXMax =  L;
PosBoxYMin =  {height/10};
PosBoxYMax=   {-height/10};
PosBoxZMin = 0 ;
PosBoxZMax = 0 ;

Field[1] = Box;
Field[1].VIn = MeshSizeInsideBox; 
Field[1].VOut = MeshSizeOutsideBox; 
Field[1].XMin = PosBoxXMin;
Field[1].XMax = PosBoxXMax;
Field[1].YMin = PosBoxYMin;
Field[1].YMax = PosBoxYMax;
Field[1].ZMin = PosBoxZMin;
Field[1].ZMax = PosBoxZMax;
Field[1].Thickness = 2.5e-3;
Background Field = 1;

"""
)

    ret = subprocess.run(f"""gmsh -2 -format msh2 -o {output_file} {input_file}""", shell=True)
    if ret.returncode:
        print("Beware, gmsh could not run: mesh is not regenerated")
    else:
        print("Mesh generated")

    
    meshconvert.convert2xml(output_file, 
                            mesh_filename, 
                            iformat='gmsh')


def create_structured_mesh(length, height, lc, min_elem,
                input_file, output_file, mesh_filename):
    open(input_file, 'w').write(
    f"""
h = 1;

x = {length}; // meters
y = {height}; // meters

a = {lc};  //meters

epsilon = 1e-5;

element_size = {min_elem}; //meters

nb_points_on_strong = (x-a)/element_size + 1;
nb_points_on_crack = a/element_size + 1;
nb_points_on_top = nb_points_on_strong + nb_points_on_crack - 1;
nb_points_on_side = (y)/element_size/2 + 1;


Point(5) = {{0,  epsilon, 0, h}};
Point(8) = {{0,  -epsilon, 0, h}};

Point(9) = {{a, 0, 0, h}};
Point(10) = {{0, y/2, 0, h}};
Point(11) = {{0, -y/2, 0, h}};
Point(12) = {{x, y/2, 0, h}};
Point(13) = {{x, -y/2, 0, h}};
Point(14) = {{x, 0, 0, h}};

Line(3) = {{10, 12}};

Line(5) = {{10, 5}};
Line(6) = {{5, 9}};
Line(7) = {{9, 14}};
Line(8) = {{14, 12}};


Line(11) = {{8, 11}};
Line(12) = {{11, 13}};
Line(13) = {{13, 14}};
Line(14) = {{9, 8}};


Line Loop(2) = {{5, 6, 7, 8, -3}};
Line Loop(4) = {{-7, 14, 11, 12, 13}};

Plane Surface(2) = {{2}};
Plane Surface(4) = {{4}};

Physical Line(2) = {{7}}; 
Physical Surface(1) = {{2, 4}};

Transfinite Line {{7}} = nb_points_on_strong Using Progression 1;
Transfinite Line {{6, 14}} = nb_points_on_crack Using Progression 1;
Transfinite Line {{3, 12}} = nb_points_on_top Using Progression 1;
Transfinite Line {{5, 8, 11, 13}} = nb_points_on_side Using Progression 1;


Transfinite Surface "*";
Transfinite Surface {{2}} = {{12, 10, 5, 14}};
Transfinite Surface {{4}} = {{14, 8, 11, 13}};

"""
)

    ret = subprocess.run(f"""gmsh -2 -format msh2 -o {output_file} {input_file}""", shell=True)
    if ret.returncode:
        print("Beware, gmsh could not run: mesh is not regenerated")
    else:
        print("Mesh generated")

    
    meshconvert.convert2xml(output_file, 
                            mesh_filename, 
                            iformat='gmsh')

if __name__ == "__main__":
    import argparse
    import os
    import numpy as np

    parser = argparse.ArgumentParser()
    parser.add_argument('--prefactor',         type=float, default=1.1)
    parser.add_argument('--nb_elem_coh',       type=float, default=10)
    parser.add_argument('--time',              type=float, default=1e-2)
    parser.add_argument('--E',                 type=float, default=106e3)
    parser.add_argument('--poisson',           type=float, default=0.35)
    parser.add_argument('--Gamma',             type=float, default=5)
    parser.add_argument('--rho',               type=float, default=1025)
    parser.add_argument('--prestrain',         type=float, default=6e3)
    parser.add_argument('--sigma_c',           type=float, default=1e4)
    parser.add_argument('--time_factor',       type=float, default=0.1)
    parser.add_argument('--alpha',             type=float, default=100)
    parser.add_argument('--length_factor',     type=float, default=10)
    parser.add_argument('--height_factor',     type=float, default=20)
    parser.add_argument('--mesh'        ,      type=str,   default='structured')
    parser.add_argument('--deformations',      type=str,   default='small')
    parser.add_argument('--material_law',      type=str,   default='linear')
    parser.add_argument('--idx',               type=str,   default='small-deformations')
    parser.add_argument('--path',              type=str,   default='./')
    parser.add_argument('--schema',            type=str,   default='trial') 
    parser.add_argument('--file',              type=open,  action=LoadFromFile)
    args = parser.parse_args()


    path = args.path  + '/' +  args.schema  + '/' + args.idx + '/'
    if not os.path.exists(path):
        os.makedirs(path)

    input_file       = path + 'mesh.geo'
    output_file      = path + 'mesh.msh'
    mesh_filename    = path + 'mesh.xml'

    # material and fracture properties
    sigma_inf = args.prestrain*args.E
       
    matprop = sm.LinearElasticMaterial({sm.smd.E:  args.E, 
                                        sm.smd.nu: args.poisson, 
                                        sm.smd.rho: args.rho, 
                                        sm.smd.pstress:False})

    # plane stress vs. plane strain
    E_eff = matprop[sm.smd.E]
    if matprop.is_plane_strain():
        E_eff /= (1-matprop[sm.smd.nu]**2)

    # Griffith's length
    lc = lefm.compute_critHalfLength(args.Gamma, sigma_inf, matprop=matprop)
    print('Griffiths length | {}'.format(lc))

    lc = args.prefactor*lc

    
    # Process zone size
    iface = {sm.smd.Gamma: args.Gamma,
             sm.smd.tauc: args.sigma_c,
             sm.smd.taur:0.0}
    lcoh = lefm.compute_Xc0(matprop, iface)
    print('Cohesive zone | {}'.format(lcoh))
    
    min_elem = lcoh/args.nb_elem_coh    
    print('Minimum element | ', min_elem)

    length = args.length_factor*lc
    height = args.height_factor*lc # was 2 times for correct runs

    if args.mesh == 'structured':
        create_structured_mesh(length=length, height=height, lc=lc,
                               min_elem=min_elem,
                               input_file=input_file,
                               output_file=output_file,
                               mesh_filename=mesh_filename)

    elif args.mesh == 'unstructured':
        max_elem = 10*min_elem
        create_unstructured_mesh(length=length, height=height, lc=lc,
                                 min_elem=min_elem,
                                 input_file=input_file,
                                 output_file=output_file,
                                 mesh_filename=mesh_filename)
