import subprocess
import uuid
import itertools
import h5py
import os

import sys

base_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")
sys.path.append(os.path.join(base_dir, "external/database-manager/"))
from dbmanager import Runner

scripts_dir = os.path.dirname(os.path.abspath(__file__))

# path where results will be stored
path = os.path.join(base_dir, "output")

# name of the study
schema = "nonlinear"

runfile_1 = os.path.join(scripts_dir, "create_mesh.py")
runfile_2 = os.path.join(scripts_dir, "cohesive_zone.py")


def create_mpirun_sbatch_script(
    file_to_run, input_file, idx, job_folder, nb_nodes=1, nb_tasks=16, cpus_per_task=2
):
    nb_tasks_per_node = int(nb_tasks / nb_nodes)
    script = f"""#!/bin/bash

#SBATCH --ntasks={nb_tasks}
#SBATCH --nodes={nb_nodes}
#SBATCH --cpus-per-task={cpus_per_task}
#SBATCH --ntasks-per-node={nb_tasks_per_node}
#SBATCH --time=04:00:00
#SBATCH --job-name={idx}
#SBATCH --mem-per-cpu=4096
#SBATCH --output={job_folder}/analysis.out
#SBATCH --error={job_folder}/analysis.err
export DIJITSO_CACHE_DIR={job_folder}/.cache/
mpirun python {file_to_run} --file {input_file}
"""
    job_script_name = job_folder + "/job-parallel.sh"
    open(job_script_name, "w").write(script)

    return job_script_name


def create_sbatch_script(file_to_run, input_file, idx, job_folder, nb_nodes=1):
    script = f"""#!/bin/bash

#SBATCH -n {nb_nodes}
#SBATCH --time=04:00:00
#SBATCH --job-name={idx}
#SBATCH --mem-per-cpu=16384
#SBATCH --output={job_folder}/analysis.out
#SBATCH --error={job_folder}/analysis.err
python {file_to_run} --file {input_file}
"""
    job_script_name = job_folder + "/job-serial.sh"
    open(job_script_name, "w").write(script)

    return job_script_name


runner = Runner()
runner.param["prefactor"] = [1.0]
runner.param["nb_elem_coh"] = [30]  # m
runner.param["E"] = [106e3]  # Pa
runner.param["poisson"] = [0.35]
runner.param["Gamma"] = [15]  # J/m2
runner.param["rho"] = [1025]  # kg/m3
runner.param["prestrain"] = [0.125]
runner.param["sigma_c"] = [2e4]
runner.param["time"] = [6e-2]
runner.param["time_factor"] = [0.05]
runner.param["alpha"] = [200]
runner.param["length_factor"] = [25]
runner.param["height_factor"] = [20]
runner.param["deformations"] = ["large", "small"]
runner.param["material_law"] = ["linear"]
runner.param["mesh"] = ["structured"]

schema_folder = path + "/" + schema
if not os.path.exists(schema_folder):
    os.makedirs(schema_folder)

for param in runner.createParametricSpace():

    idx = uuid.uuid4().hex
    job_folder = schema_folder + "/" + idx
    if not os.path.exists(job_folder):
        os.makedirs(job_folder)
        os.makedirs(job_folder + "/.cache")

    arguments_file = ""
    for key, parameters in zip(runner.param.keys(), param):
        arguments_file += "--{} {} ".format(key, str(parameters))

    arguments_file += "--idx {} ".format(idx)
    arguments_file += "--path {} ".format(path)
    arguments_file += "--schema {} ".format(schema)

    arguments_filename = job_folder + "/arguments.dat"
    open(arguments_filename, "w").write(arguments_file)

    job_script_name = create_sbatch_script(
        file_to_run=runfile_1,
        idx=idx,
        job_folder=job_folder,
        input_file=arguments_filename,
    )

    result = subprocess.run(
        "sbatch --parsable {}".format(job_script_name),
        capture_output=True,
        text=True,
        shell=True,
        env=os.environ,
    ).stdout.strip("\n")

    print("Job | ", result)

    job_script_name = create_mpirun_sbatch_script(
        file_to_run=runfile_2,
        idx=idx,
        job_folder=job_folder,
        input_file=arguments_filename,
    )

    myjobid = subprocess.run(
        "sbatch --dependency=afterany:{} --parsable {}".format(result, job_script_name),
        capture_output=True,
        text=True,
        shell=True,
        env=os.environ,
    ).stdout.strip("\n")

    print("Job | ", myjobid)

    schema_file = "schema_space.h5"
    filename = path + "/" + schema + "/" + schema_file

    with h5py.File(filename, "a") as h5file:
        grp = h5file.create_group(str(idx))
